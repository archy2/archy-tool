install:
	cp archy.sh /usr/local/bin/archy
	cp archy.desktop /usr/share/applications/

uninstall:
	rm /usr/local/bin/archy
