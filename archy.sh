#!/usr/bin/env bash

#Creator: Dillant
#Dependencies: libnewt, fzf
#Purpose: for people who are new to arch linux

#set cancel button
c_b="--cancel-button Back"
#set back text
back="Archy, By Dillon King, Version: 0.4"


# checking for the config folder and file and creating if they don't exist
    [[ ! -d /home/$USER/.config/archy-tool ]] && mkdir /home/$USER/.config/archy-tool
    [[ ! -f /home/$USER/.config/archy-tool/config ]] && cp /opt/archy-tool/config /home/$USER/.config/archy-tool/config
    # Load the config
    DONATE=$(grep donate /home/$USER/.config/archy-tool/config | awk '{print $2}')
    COLOR=$(grep color /home/$USER/.config/archy-tool/config | awk '{print $2}')
    UNIT=$(grep temp_unit /home/$USER/.config/archy-tool/config | awk '{print $2}')


    #change whiptail colors
    export NEWT_COLORS="
           root=,$COLOR
           "

function main {
    clear

        # set the main menu for archy
    options=()
    options+=("Update the system" "")
    options+=("Applications" "")
    options+=("Shell" "")
    options+=("Manage Users" "")
    options+=("Mount or Unmount Drives" "")
    options+=("Build custom backup script(Experimental)" "")
    options+=("run backup script" "")
    options+=("AUR Helper" "")
    options+=("Weather" "")
    options+=("SDDM" "")
    # only put donate button if set to true in config file
    [[ $DONATE == "true" ]] && options+=("Donate" "")

    # run the main menu
    app=$(whiptail --title "Archy"  --backtitle "$back" --menu "What would you like to do?" 0 0 0 \
        "${options[@]}" \
         3>&1 1>&2 2>&3)

    # load or functions based on selected item
    if [[ $app == "Update the system" ]]; then
        update
    elif [[ $app == "Update Mirrors(Not working)" ]]; then
          #updatemirrors
          notworking
    elif [[ $app == "Applications" ]]; then
        applications
    elif [[ $app == "Shell" ]]; then
        shellmenu
    elif [[ $app == "Manage Users" ]]; then
        users
    elif [[ $app == "Mount or Unmount Drives" ]]; then
        drives
    elif [[ $app == "Build custom backup script(Experimental)" ]]; then
        source /opt/archy-tool/scripts/makecustombackup.sh
    elif [[ $app == "run backup script" ]]; then
        runbackup
    elif [[ $app == "AUR Helper" ]]; then
        aur
    elif [[ $app == "Weather" ]]; then
        weather
    elif [[ $app == "SDDM" ]]; then
        sddm
    elif [[ $app == "Donate" ]]; then
        xdg-open https://www.buymeacoffee.com/archytool
        whiptail --title "Archy" --msgbox "Thanks for your support!!!" 0 0
        main
    fi

}

function notworking {
    # tell the user the feature is not working
    whiptail --title "Archy" --backtitle "$back" $c_b --msgbox "This feature is currently not working sorry for the inconvenience" 0 0
    main
}

function update {
clear
echo "#######################"
echo "# Updating the System #"
echo "#######################"

[[ -f /bin/paru ]] && paru
[[ ! -f /bin/paru ]] && sudo pacman -Syu
main
}

function sddm {
    theme=$(grep Current /etc/sddm.conf.d/kde_settings.conf | awk -F "=" '{print $NF}')
    local options=()
    local options+=("Change theme" "")
    local options+=("Install a new theme" "")
    sddmoption=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu "Current SDDM theme is: $theme" 0 0 0 \
        "${options[@]}" \
        3>&1 1>&2 2>&3)
    if [[ $? == 1 ]]; then
        main
    else
        case $sddmoption in
            "Change theme")
                sddmtheme=$(grep Current /etc/sddm.conf.d/kde_settings.conf | awk -F "=" '{print $NF}')
                themesls=$(command ls /usr/share/sddm/themes/)
                themes=()
                for theme in $themesls; do
                    themes+=("$theme" "")
                done
                newtheme=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu "Please select a new theme" 0 0 0 \
                    "${themes[@]}" \
                    3>&1 1>&2 2>&3)
                if [[ $? -eq 1 ]]; then
                    sddm
                else
                    sudo sed -i "s/$sddmtheme/$newtheme/g" /etc/sddm.conf.d/kde_settings.conf
                    sddm
                fi
                ;;
            "Install a new theme")
                if [[ ! -f /bin/paru ]]; then
                    whiptail --title "Archy" --backtitle "$back" --msgbox "You need to have paru installed to do this you can install it in the AUR helper menu" 0 0
                    sddm
                else
                    themesls=$(command ls /usr/share/sddm/themes/)
                    themes=()
                    for theme in $themesls; do
                        themes+=("$theme" "")
                    done
                    unset themeslist
                    installthemes=(sddm-sugar-dark)
                    for itheme in ${installthemes[@]}
                    do
                        if [[ -d /usr/share/sddm/themes/$itheme ]]; then
                            blist+=($itheme "                                                           " "ON")
                        else
                            blist+=($itheme "                                                           " "OFF")
                        fi
                    done
                    browser=$(whiptail  --title "Archy" $c_b --backtitle "$back" --checklist "Select SDDM themes, anything checked is already installed." 0 0 0 \
                        "${blist[@]}" \
                        3>&1 1>&2 2>&3)
                    if [[ $? == 1 ]]; then
                        sddm
                    else
                        browser=$(echo $browser | sed 's/"//g')
                        paru -S --needed --noconfirm $browser
                        main
                    fi
                fi
                ;;
        esac
    fi
}

function shellmenu {
    shelloptions=()
    shelloptions+=("List shells on system" "")
    shelloptions+=("Change shell" "")
    shelloptions+=("Enable custom shell options" "")
    shelloption=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu "Current shell is $SHELL" 0 0 0 \
        "${shelloptions[@]}" \
        3>&1 1>&2 2>&3)
    if [[ $? == 1 ]]; then
        main
    else
        case $shelloption in
            "List shells on system")
                SHELLS=$(grep -v "#" /etc/shells | awk -F "/" '{print $NF}' )
                whiptail --title "Archy" --backtitle "$back" $c_b --msgbox "$SHELLS" 0 0
                shellmenu
                ;;
            "Change shell")
                SHELLS=$(grep -v "#" /etc/shells)
                shellslist=()
                for shell in $SHELLS; do
                    shellslist+=($shell "")
                done
                newshell=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu "Please choose your new shell" 0 0 0 \
                    "${shellslist[@]}" \
                    3>&1 1>&2 2>&3)

                if [[ $? == 1 ]]; then
                    shellmenu
                else
                    chsh -s $newshell
                    whiptail --title "Archy" --backtitle "$back" --msgbox "You need to logout and log back into for chnages to apply" 0 0
                    shellmenu
                fi
                ;;
            "Enable custom shell options")
                whiptail --title "Archy" --backtitle "$back" --yesno "These options only support Bash would you like to continue?" 0 0
                if [[ $? == 1 ]]; then
                  shellmenu
                else
                    shoptopt=()
                    shoptopt+=("Auto CD" "")
                    shoptopt+=("CD Spell" "")
                    shelloption=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu "What shell option would you like to enable?" 0 0 0 \
                        "${shoptopt[@]}" \
                        3>&1 1>&2 2>&3)
                    if [[ $? == 1 ]]; then
                        shellmenu
                    else
                        [[ $shelloption == "Auto CD" ]] && echo "shopt -s autocd" >> /home/$USER/.bashrc
                        [[ $shelloption == "CD Spell" ]] && echo "shopt -s cdspell" >> /home/$USER/.bashrc
                        shellmenu
                    fi
                fi
                ;;
        esac
    fi
}

function aur {
    [[ -f /bin/paru ]] && selected="AUR Helper: Paru" && paruinstalled=0
    [[ ! -f /bin/paru ]] && selected="No aur helper" && paruinstalled=1

    auroptions=()
    [[ $paruinstalled == 1 ]] && auroptions+=("Install Paru" "")
    auroptions+=("Fix Paru" "")


    aurmenu=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu "$selected" 0 0 0 \
        "${auroptions[@]}" \
        3>&1 1>&2 2>&3)

    if [[ $? == 1 ]]; then
        main
    else
        [[ $aurmenu == "Install Paru" ]] && installparu
        [[ $aurmenu == "Fix Paru" ]] && parufix
        main
    fi
}

function installparu {
   cd /home/$USER/
   sudo pacman -S --needed base-devel
   git clone https://aur.archlinux.org/paru.git
   cd paru
   makepkg -si
}

function runbackup {
    local path="/home/$USER/.config/archy-tool/backup.sh"
    # check if the user has made a backup script with archy if not they are prompted to
    [[ ! -f $path ]] && whiptail --title "Archy" --backtitle "$back" --msgbox "you must make a custom backup script first" 0 0 && main

    [[ -f $path ]] && [[ ! -x $path ]] && whiptail --title "Archy" --backtitle "$back" --msgbox "File is not executable" 0 0 && main
    [[ -x $path ]] && source $path

}

function updatemirrors {
    unset clist
    countrys=(France Germany United_Kingdom)
    for i in ${countrys[@]}; do
        clist+=($i "")
    done

    country=$(whiptail --title "Archy" $c_b --backtitle "$back" --menu "What country would you like to pick?" 0 0 0 \
        "${clist[@]}" \
        3>&1 1>&2 2>&3)

    country=$($country | sed 's/_/ /g')

    if [[ $? == 1 ]]; then
        main
    else
        sudo reflector --latest 20 --fastest 5 --protocol https --download-timeout 60 --country '$country' --sort rate --save /etc/pacman.d/mirrorlist
    fi

    main
}

function uninstallapps {
    # run the app uninstall script in scripts/uninstall.sh
    clear
    source /opt/archy-tool/scripts/uninstallapps.sh
}

function installapps {
    # load the main menu for app install
    clear
    cata=$(whiptail  --title "Archy" $c_b --backtitle "$back" --menu "Select A Catagorie" 0 0 0 \
        "Browsers" "" \
        "Office Apps" "" \
        "Media" "" \
        "Text Editors" "" \
        "Terminals" "" \
        "Utils" "" \
        "Gaming" "" \
        "Window Managers" "" \
        "Laptop" "" \
        "Extras" "" \
        3>&1 1>&2 2>&3)

    # load relevent app install function based on what the user selected
    if [[ $? == 1 ]]; then
       applications
    else
        case $cata in
            "Browsers")
                appsbrowsers
                ;;
            "Office Apps")
                appsoffice
                ;;
            "Media")
                appsmedia
                ;;
            "Text Editors")
                appstexteditors
                ;;
            "Terminals")
                appsterminal
                ;;
            "Utils")
                appsutils
                ;;
            "Gaming")
                appsgaming
                ;;
            "Window Managers")
                appswm
                ;;
            "Laptop")
                appslaptop
                ;;
            "Extras")
                appsextras
                ;;
        esac


    fi
}

function applications {
        appschoice=$(whiptail --title "Archy" $c_b --backtitle "$back" --menu "What would you like to do?" 0 0 0 \
                "Install an app" "" \
                "Uninstall an app" "" \
                "Create a package list" "" \
                "Install from a package list" "" \
                3>&1 1>&2 2>&3)

    if [[ $? == 1 ]]; then
        main
    else
        case $appschoice in
            "Install an app")
                    installapps
                    ;;
            "Uninstall an app")
                uninstallapps
                ;;
            "Create a package list")
                packagelist
                ;;
            "Install from a package list")
                installpackagelist
                ;;
        esac

    fi
}

function packagelist {
    [[ -f /home/$USER/.config/archy-tool/pkglist.txt ]] && rm /home/$USER/.config/archy-tool/pkglist.txt
    if [[ -f /bin/paru ]]; then
        paru -Qqe > /home/$USER/.config/archy-tool/pkglist.txt
    else
        pacman -Qqe > /home/$USER/.config/archy-tool/pkglist.txt
    fi
    main
}

function installpackagelist {
    if [[ ! -f /home/$USER/.config/archy-tool/pkglist.txt ]]; then
        whiptail --title "Archy" --backtitle "$back" --msgbox "You need to have the pkglist in ~/.config/archy-tool/" 0 0 && main
    else
        if [[ -f /bin/paru ]]; then
            paru -S --needed - < /home/$USER/.config/archy-tool/pkglist.txt
        else
            sudo pacman -S --needed $(comm -12 <(pacman -Slq | sort) <(sort /home/$USER/.config/archy-tool/pkglist.txt))
        fi
    fi
    main
}


function appsterminal {
        browsers=(alacritty kitty gnome-terminal xfce4-terminal konsole xterm)
        appsinstall ${browsers[@]}
}

function appsextras {
    extra=$(whiptail --title "Archy" $c_b --backtitle "$back" --menu "Please select an extra to install" 0 0 0 \
        "Doom Emacs" "" \
        "SpaceVim" "" \
        3>&1 1>&2 2>&3)

    if [[ $? == 1 ]]; then
        installapps
    else

        if [[ $extra == "Doom Emacs" ]]; then
            git clone --depth 1 --single-branch https://github.com/doomemacs/doomemacs ~/.config/emacs
            ~/.config/emacs/bin/doom install
        elif [[ $extra == "SpaceVim" ]]; then
            curl -sLf https://spacevim.org/install.sh | bash
        fi
    fi
}

function appsutils {
        browsers=(fzf btop htop neofetch zoxide)
        appsinstall ${browsers[@]}
}

function appslaptop {
        browsers=(tlp tlp-rdw brightnessctl)
        appsinstall ${browsers[@]}
}

function appsinstall {
        unset blist
        browsers="$1"
        for app in ${browsers[@]}
        do
            if [[ -f /bin/$app ]]; then
                blist+=($app "                                                           " "ON")
            else
                blist+=($app "                                                           " "OFF")
            fi
        done
            browser=$(whiptail  --title "Archy" $c_b --backtitle "$back" --checklist "Select browsers, anything checked is already installed." 0 0 0 \
                "${blist[@]}" \
                3>&1 1>&2 2>&3)
            if [[ $? == 1 ]]; then
                installapps
            else
                browser=$(echo $browser | sed 's/"//g')
                sudo pacman -S --needed --noconfirm $browser
                main
            fi
}

function appsbrowsers {
    browsers=(qutebrowser brave firefox chromium)
    appsinstall ${browsers[@]}
}

function appsoffice {
        browsers=(libreoffice)
        appsinstall ${browsers[@]}
}

function appsmedia {
        browsers=(vlc mpv celluloid spotify obs shotcut)
        appsinstall ${browsers[@]}
}

function appstexteditors {
        browsers=(vim emacs neovim gedit kate micro nano)
        appsinstall ${browsers[@]}
}


function appsgaming {
        browsers=(steam lutris)
        appsinstall ${browsers[@]}
}


function appswm {
        browsers=(herbstluftwm awesome xmonad)
        appsinstall ${browsers[@]}
}

function userinfo {
    # gets a list of all users and puts them in a whiptail message box
    userslist=$(cat /etc/passwd | awk -F ":" '{if ($3 >= 1000) print $1}' | grep -v "nobody")
    whiptail --title "Archy" $c_b --backtitle "$back" --msgbox "$userslist" 0 0 3>&1 1>&2 2>&3
    if [[ $? == 1 ]]; then
        users
    else
        users
    fi

}

function addtogroup {
             clear
             unset dusers
             users=$(cat /etc/passwd | awk -F ":" '{if ($3 >= 1000) print $1}' | grep -v "nobody" )
             for i in ${users[@]}
             do
                     dusers+=($i "")
             done
             deluser=$(whiptail --title "Archy" $c_b --menu "Select a user to add to a group" 0 0 0 \
                 "${dusers[@]}"  \
                 3>&1 1>&2 2>&3)

             if [[ $? == 1 ]]; then
                 users
             else

                 group=$(whiptail --inputbox "What group would you like to add $deluser to?" 0 0 --title "Archy" $c_b --backtitle "$back" 3>&1 1>&2 2>&3)

                 if [[ $? == 1 ]]; then
                     users
                 else
                     sudo usermod -a -G $group $deluser
                 fi
             fi
}

function adduser {
             name=$(whiptail --title "Archy" $c_b --inputbox "What you like to call your user?" 0 0 \
            3>&1 1>&2 2>&3)
             if [[ $? == 1 ]]; then
                 users
             else
                 sudo useradd $name
                 pass=$(whiptail --passwordbox "please enter your password for $name" 0 0 --title "Archy" 3>&1 1>&2 2>&3)
                 sudo usermod -a -G wheel,video,audio $name
                 (echo $pass; echo $pass;) | sudo passwd $name
                 whiptail --title "Archy" --backtitle "$back" --yesno "Would you like to make a home directory for $name ?" 0 0
                 case $? in
                     0)
                         sudo mkdir /home/$name
                         ;;
                 esac
                 main
             fi
}

function deluser {
             dusers=0
             dusers=()
             users=$(cat /etc/passwd | awk -F ":" '{if ($3 >= 1000) print $1}' | grep -v "nobody" )
             for i in ${users[@]}
             do
                     dusers+=($i "")
             done
             deluser=$(whiptail --title "Archy" $c_b --menu "Select a user to delete" 0 0 0 \
                 "${dusers[@]}"  \
                 3>&1 1>&2 2>&3)
             if [[ $? == 1 ]]; then
                 users
             else
                 for i in $users
                 do
                     if [[ $i == $deluser ]]; then
                         correct="t"
                     elif [[ $correct == "t" ]]; then
                         correct="t"
                     fi
                 done

                 if [[ $correct != "t" ]]; then
                     whiptail --title "Archy" --backtitle "$back" --msgbox "Thats not a user, try again" 0 0 3>&1 1>&2 2>&1
                     users

                 else
                     whiptail --title "Archy" $c_b --yesno "Would you like to delete the users home directory" 0 0 \
                         3>&1 1>&2 2>&3
                     case $? in
                         0)
                             sudo userdel -r $deluser
                             ;;
                         1)
                             sudo userdel $deluser
                             ;;
                     esac
                     users
                 fi
             fi
}

function users {
    clear
    choice=$(whiptail --title "Archy" $c_b  --backtitle "$back" --menu "What would you like to do?" 0 0 0 \
        "Add a user" "" \
        "Remove a user" "" \
        "List users" "" \
        "Add user to group" "" \
         3>&1 1>&2 2>&3)

    if [[ $? == 1 ]]; then
        main

    else
        case $choice in
            "Add a user")
                adduser
                ;;
            "Remove a user")
                deluser
                ;;
            "List users")
                userinfo
                ;;
            "Add user to group")
                addtogroup
                ;;
        esac
    fi
}

function drives {
    drivechoice=$(whiptail --title "Archy" --backtitle "$back" $C_b --menu "Would you to mount or unmount a drive?" 0 0 0 \
        "Mount" "" \
        "Unmount" "" \
        3>&1 1>&2 2>&3)
    if [[ $? -eq 1 ]]; then
        main
    else

        case $drivechoice in
            "Mount")
                drives=$(lsblk -lp | grep "part $" | awk '{print $1}')
                dmount=$(printf "%s\n" "${drives[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Drive to mount: ")
                [[ -z $dmount ]] && exit 1
                # directorys can be added
                dirs=$(find /mnt -maxdepth 3 2>/dev/null)

                mountpoint=$(printf "%s\n" "${dirs[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Mounpoint: ")

                sudo mount $dmount $mountpoint
                if [[ $? -eq 0 ]]; then
                    whiptail --msgbox "Drive mounted" 0 0
                    main
                elif [[ $? -ne 0 ]]; then
                    whiptail --msgbox "Drive not mounted" 0 0
                    main
                fi

                ;;
            "Unmount")
                # I only mount drives to /mnt so only look there
                drives=$(lsblk -lp | grep "t /" | awk '{print $NF}' | grep "mnt")
                udrive=$(printf "%s\n" "${drives[@]}" | rofi -dmenu -i -l 20 -fn 11 -sb purple -nb '#282A36' -p "Drives to unmount: ")
                [[ -z $udrive ]] && exit 1
                sudo umount $udrive
                if [[ $? -eq 0 ]]; then
                    whiptail --msgbox "Drive unmounted" 0 0
                    main
                elif [[ $? -ne 0 ]]; then
                   whiptail --msgbox "Drive not unmounted" 0 0
                   main
                fi
                ;;
        esac
    fi
}

function weather {
    [[ $UNIT == "c" ]] && tempc=$(wget -qO- "http://wttr.in/?format=j1" | jq -r ."current_condition[0]."temp_C"")
    [[ $UNIT == "f" ]] && tempf=$(wget -qO- "http://wttr.in/?format=j1" | jq -r ."current_condition[0]."temp_F"")

    if [[ $UNIT == "c" ]]; then
        tempmessage="Temp in C: $tempc"
    else
        tempmessage="Temp in F: $tempf"
    fi

    wcondition=$(wget -qO- "http://wttr.in/?format=j1" | jq -r ."current_condition[0]."weatherDesc[0]."value""")
    weathermenu=()
    weathermenu+=("See full view" "")
    weatheropt=$(whiptail --title "Archy" --backtitle "$back" $c_b --menu " The Current weather is
    $tempmessage
    Condition: $wcondition" 0 0 0 \
        "${weathermenu[@]}" \
        3>&1 1>&2 2>&3)
    if [[ $weatheropt == "See full view" ]]; then
        wget -qO- "http://wttr.in/"
        read -n 1 -s -r -p "Press any key to continue"
    fi
    main
}

function parufix {
    echo "###############"
    echo "# Fixing Paru #"
    echo "###############"
    sleep 3
    # fixes missing paru libary
    sudo ln -s /usr/lib/libalpm.so.14.0.0 /usr/lib/libalpm.so.13
    main
}

# check for flags

case $# in
    0)
        main
        ;;
    1)
        case $1 in
            "--about")
                echo -e "\033[4mAbout:\033[0m"
                echo "   Version: 0.4"
                echo "   Background Colour: $COLOR"
                if [[ $DONATE == "true" ]]; then
                    echo "   Donate button is on"
                else
                    echo "   Donate button is off"
                fi

                if [[ $UNIT == "c" ]]; then
                    echo "   Temperature is in Celsius"
                else
                    echo "   Temperature is in Fahrenheit"
                fi
                ;;
        esac
        ;;
esac


