#+title: Readme
#+author: Dillon King
#+startup: inlineimages
[[file:assets/archy.png]]
* Install
to install please go to archys website: [[https://archy-tool.xyz]]

* Config file
you can find this at: ~/.config/archy-tool/config

** Options
leave a space and input you desired background color after the colon
#+begin_src bash
bgcolor: red
#+end_src

Set to true to enable donate button or false to disable it
#+begin_src bash
show_donate_button: true
#+end_src
