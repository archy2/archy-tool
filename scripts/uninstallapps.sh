#!/usr/bin/env bash

#define functions

function warning {
    whiptail --title "Archy" --yesno "If you uninstall an app and it breaks the OS we are not responsible, would you like to continue?" 0 0
    [[ $? == 1 ]] && archy && exit 0
}

function applist {
    app=$(pacman -Qqe | fzf -i --layout "reverse" --prompt "App to uninstall: " --border "rounded")
    [[ $app == "" ]] && archy && exit 1
}

function uninstall {
    sudo pacman -R $app
}
# show warning
warning

# list the apps into fzf
applist

# uninstall
uninstall

# go back to Archy
archy
