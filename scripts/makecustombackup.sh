#!/usr/bin/env bash



# delete backup script if it already exists
[[ -f /home/$USER/.config/archy-tool/backup.sh ]] && rm /home/$USER/.config/archy-tool/backup.sh

#set cancel button
c_b="--cancel-button Back"
#set back text
back="Archy, By Dillon King, Version: 0.0.2"

#change whiptail colors
COLOR=$(cat /home/$USER/.config/archy-tool/config | grep color | awk '{print $NF}')

export NEWT_COLORS="
          root=,$COLOR
          "

function main {
    touch /home/$USER/.config/archy-tool/backup.sh
    chmod +x /home/$USER/.config/archy-tool/backup.sh


    cat <<EOF>> /home/$USER/.config/archy-tool/backup.sh
    # make sure they don't exit accidently

    function controlc {
        whiptail --yesno "Are you sure you want to exit?" 0 0
        [[ $? == 0 ]] && exit 0
    }

    # trap exit signal
    trap "controlc" SIGINT
EOF

    whiptail --title "Archy" --backtitle "$back" --yesno "Are you going to put backup on and external drive?" 0 0
    if [[ $? == 1 ]]; then
        whiptail --title "Archy" --backtitle "$back" --msgbox "Ok will store in /var/backup" 0 0
        external=1
    else
        whiptail --title "Archy" --backtitle "$back" --msgbox "Ok the script will ask you for the mounpoint of device when you run it" 0 0
        external=0
    fi

    case $external in
       1)
           echo "#!/usr/bin/env bash" >> /home/$USER/.config/archy-tool/backup.sh
           sudo mkdir /var/backup
           ;;
       0)
           echo "#!/usr/bin/env bash" >> /home/$USER/.config/archy-tool/backup.sh
           echo "#ask for mountpoint of external drive" >> /home/$USER/.config/archy-tool/backup.sh
           echo "lsblk" >> /home/$USER/.config/archy-tool/backup.sh
           echo "echo "What is the mountpoint of your external drive" && read drive" >> /home/$USER/.config/archy-tool/backup.sh
           ;;
    esac
    whiptail --title "Archy" --backtitle "$back" --yesno "Do you want to back up your gpg keys in the script?" 0 0
    if [[ $? == 0 ]]; then
        gnupg
    fi

    whiptail --title "Archy" --backtitle "$back" --yesno "Do you use the pass app on your system?" 0 0
    if [[ $? == 0 ]]; then
        password-store
    fi

    whiptail --title "Archy" --backtitle "$back" --yesno "Do you want to back up any directorys" 0 0
    if [[ $? == 0 ]]; then
        directory
    fi



}

function gnupg {
    if [[ $external == 0 ]]; then
       echo "# backup gpg keys" >> /home/$USER/.config/archy-tool/backup.sh
       echo "sudo cp -r ~/.gnupg \$drive" >> /home/$USER/.config/archy-tool/backup.sh
    elif [[ $external == 1 ]]; then
       echo "# backup gpg keys" >> /home/$USER/.config/archy-tool/backup.sh
       echo "sudo cp -r ~/.gnupg /var/backups" >> /home/$USER/.config/archy-tool/backup.sh
    fi


}

function password-store {
    if [[ $external == 0 ]]; then
        echo "# backup passwords" >> /home/$USER/.config/archy-tool/backup.sh
        echo "sudo cp -r ~/.password-store \$drive" >> /home/$USER/.config/archy-tool/backup.sh
    elif [[ $external == 1 ]]; then
        echo "# backup passwords" >> /home/$USER/.config/archy-tool/backup.sh
        echo "sudo cp -r ~/.password-store /var/backup" >> /home/$USER/.config/archy-tool/backup.sh
    fi

}

function directory {
    echo "DATE=\$(date +%F)" >> /home/$USER/.config/archy-tool/backup.sh
   name=$(whiptail --title "Archy" --backtitle "$back" --inputbox "Please type the name of folder without the path" 0 0 \
       3>&1 1>&2 2>&3)
   x=0
   while [[ $x != 1 ]]
   do
       dir=$(whiptail --title "Archy" --backtitle "$back" --inputbox "Please type full path to the directory you would like to backup" 0 0 \
           3>&1 1>&2 2>&3)
       if [[ $(echo $dir | awk -F "/" '{print $1}') == "~" ]]; then
           end=$(echo $dir | awk -F "~" '{print $2}')
           dir="/home/$USER$end"
       fi
       if [[ $? == 1 ]]; then
           exit 0
       else
           if [[ -d $dir ]]; then
               echo "name_$name=$name" >> /home/$USER/.config/archy-tool/backup.sh
               echo "NAME="\$name_$name\$DATE >> /home/$USER/.config/archy-tool/backup.sh
               echo "# make backup of $name" >> /home/$USER/.config/archy-tool/backup.sh
               echo "tar cvzf $dir \$NAME.tar.gz"  >> /home/$USER/.config/archy-tool/backup.sh
               echo "sudo mv \$NAME.tar.gz \$drive" >> /home/$USER/.config/archy-tool/backup.sh
               whiptail --title "Archy" --backtitle "$back" --yesno "Would you like to backup another directory?" 0 0
               [[ $? == 1 ]] && x=1
           elif [[ ! -d $dir ]]; then
               whiptail --title "Archy" --backtitle "$back" --msgbox "That directory does not exist please try again" 0 0
           fi
       fi
    done
}
main

echo "archy" >> /home/$USER/.config/archy-tool/backup.sh

archy
